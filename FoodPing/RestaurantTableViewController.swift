//
//  RestaurantTableViewController.swift
//  FoodPing
//
//  Created by fred fu on 2020/9/8.
//  Copyright © 2020 fred fu. All rights reserved.
//

import UIKit

class RestaurantTableViewController: UITableViewController {

    var restaurantNames = [
        "Cafe Deadend",
        "Homei",
        "Teakha",
        "Cafe Loisl",
        "Petite Oyster",
        "For Kee Restaurant",
        "Po's Atelier",
        "Bourke Street Bakery",
        "Haigh's Chocolate",
        "Palomino Espresso",
        "Upstate",
        "Traif",
        "Graham Avenue Meats And Deil"

    ]
    
    var restaurantImages = [
        "user00",
        "user01",
        "user02",
        "user03",
        "user04",
        "user05",
        "user06",
        "user07",
        "user08",
        "user09",
        "user10",
        "user11",
        "user12"
    ]

    var restaurantIsVisited = Array(repeating: false, count: 13)

    override func viewDidLoad() {
        super.viewDidLoad()
        //PAD顯示調整
        tableView.cellLayoutMarginsFollowReadableWidth = true
    
        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        restaurantNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "datacell"
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: cellIdentifier, for: indexPath) as? RestaurantTableViewCell
            else { return RestaurantTableViewCell() }
        
        cell.nameLabel?.text = restaurantNames[indexPath.row]
        cell.thumbnailImageView?.image = UIImage(named: restaurantImages[indexPath.row])
        cell.accessoryType = restaurantIsVisited[indexPath.row]
            ? .checkmark
            : .none

        return cell
    }
    
    //實作
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let optinMenu = UIAlertController(
            title: nil,
            message: "What do you want to do?",
            preferredStyle: .alert
        )
        let cancelAction = UIAlertAction(
            title: "cancel？",
            style: .cancel,
            handler: nil
        )
        optinMenu.addAction(cancelAction)
        //撥電話之後
        let callActionHandler = { [weak self] (action: UIAlertAction) -> Void in
            
            let alertMessage = UIAlertController(
                title: "Service Unavailable",
                message: "Sorry, the call feature is not available yet. Please try later ",
                preferredStyle: .alert
            )
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self?.present(alertMessage, animated: true, completion: nil)
            
        }
        //撥電話
        let callAction = UIAlertAction(
            title: "Call " + "123-000-\(indexPath.row)",
            style: .default,
            handler: callActionHandler
        )
        optinMenu.addAction(callAction)
        
        if restaurantIsVisited[indexPath.row] {
            //取消打卡
            let uncheckInAction = UIAlertAction(
                title: "Undo Checkin",
                style: .default,
                handler: { [weak self] _ in
                    let cell = tableView.cellForRow(at: indexPath)
                    cell?.accessoryType = .none
                    self?.restaurantIsVisited[indexPath.row] = false
            })
            optinMenu.addAction(uncheckInAction)
        } else {
            //打卡
            let checkInAction = UIAlertAction(
                title: "Check in",
                style: .default,
                handler: { [weak self] _ in
                    let cell = tableView.cellForRow(at: indexPath)
                    cell?.accessoryType = .checkmark
                    self?.restaurantIsVisited[indexPath.row] = true
            })
            optinMenu.addAction(checkInAction)
        }
    
        
        //呈現選單
        present(optinMenu, animated:true, completion: nil)
        
        
    }
    
//    //左滑刪除列功能
//
//    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath){
//
//        if editingStyle == .delete {
//
//            restaurantNames.remove(at: indexPath.row)
////            restaurantLocation.remove(at: indexPath.row)
////            restaurantTypes.remove(at: indexPath.row)
//            restaurantIsVisited.remove(at: indexPath.row)
//            restaurantImages.remove(at: indexPath.row)
//        }
//
//
//        tableView.deleteRows(at: [indexPath], with: .fade)
//
//    }
    
    
    //建立往左拉會有多功能按鈕 分享功能
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {
            
            (action, sourceView, completionHandler) in
            
            self.restaurantNames.remove(at: indexPath.row)
            //self.restaurantLocation.remove(at: indexPath.row)
            //self.restaurantTypes.remove(at: indexPath.row)
            self.restaurantIsVisited.remove(at: indexPath.row)
            self.restaurantImages.remove(at: indexPath.row)
            
            self.tableView.deleteRows(at:[indexPath], with: .fade)
            
            completionHandler(true)
            
        }
        
        let shareAction = UIContextualAction(style: .normal, title: "Share") { (action, sourceView, completionHandler) in
            
            let defaultText = "Just checking in at " + self.restaurantNames[indexPath.row]
            
            let activityController: UIActivityViewController
            
            //加入imageToShare物件
            
            if let imageToShare = UIImage(named: self.restaurantImages[indexPath.row]) {
                
                activityController = UIActivityViewController (activityItems: [defaultText, imageToShare], applicationActivities: nil)
                
            } else {
                
                activityController = UIActivityViewController(activityItems: [defaultText], applicationActivities: nil)
                
            }
            
            
            
            self.present(activityController, animated: true, completion: nil)
            
            completionHandler(true)
            
        }
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction, shareAction])
        
        return swipeConfiguration
        
        
        
    }
}
